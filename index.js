// [Section] Function
	// Fuctions in Javascript are lines or blocks of codes that tell our device or application to perform a certain task when called or invoke.
	// Functions are mostly used to create a complicated tasks to run several lines of code in succession.
	// They are also used to prevent repeating lines/blocks of codes that perform the same task or function

	// Function Declaration
		// (function statements) - defines a function with specified parameters.

		/*Syntax:
			function functionName(){
				code block (statements)
			}
		*/
		// 'function' keyword - used to define a javascript functions
		// 'functionName' - the function name. these are named to be able to use later in the code.
		// function block {} - the statements which comprise the body of the function. this is where the code will be executed.

		function printName(){
			console.log("My name is John.")
			console.log("My last name is Dela Cruz.")
		}

	// Function Invocation
		// The code block and statements inside a function is not immediately executed when the function is defined or declared.
		// The code block and statements inside the function is executed when the function is invoked. 
		// It is common to use the term "call a function" instead of "invoke a function". (But we will use "invoke" most of the time)
		
		// Invoke the function that was declared.
		printName();

		// Functions are reusable. Can be invoked many times.
		printName();


// [Section] Function Declaration and Function Expression
	// Function Declaration (hoisted)
		// Function can be created through function declaration by using the keyword "function" and adding the function name.

		// Declared functions are not executed immediately.

		declaredFunction();

		function declaredFunction(){
			console.log("Hello World from declaredFunction");
		}

	// Function Expression 
		// A function can also be stored in a variable. (cannot be hoisted)

		// Anonymous Function - functions without a name. (kailangan mauna ang expression before the invocation)

		/*variableFunction();*/
		let variableFunction = function(){
			console.log("Hello from variableFunction");
		}
		variableFunction();

		// With Name

		let funcExpression = function funcName(){
			console.log("Hello from funcExpression");
		}
		funcExpression();

	// You can reassign declared functions and function expression to new anonymous function. (wala ng "let")

		declaredFunction = function(){
			console.log("Updated declaredFunction");
		}
		declaredFunction();

		funcExpression = function(){
			console.log("Updated funcExpression");
		}
		funcExpression();

	// Function expression using 'const' keyword
		const constantFunc = function(){
			console.log("Initialized with const");
		}
		constantFunc();

		// Cannot be reassigned if using 'const' keyword
			/*constantFunc = function(){
				console.log("Cannot be reassigned")
			}
			constantFunc();*/


// [Section] Function Scoping
	// Scope is accessibility of variable within our program.

	/* 	Javascript varibales has 3 types of scope
			1. Local or Block Scope
			2. Global Scope
			3. Function Scope
	*/

	// Local/Block Scope and Global Scope Example
		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}
		let globalVar = "Mr. Worldwide";
		/*console.log(localVar);*/

	// Function Scope
	// Javascript has funcstion scope. Each function creates a new scope. Variables defined inside the function are not accesible outside the function.

		/*function showNames(){
			let functionLet = "Jane";
			const functionConst = "John";

			console.log(functionConst);
			console.log(functionLet);
		}
		showNames();*/

		// The variables, functionLet and functionConst, are function scope and cannot be access outside of the function where they are declared.
		/*console.log(functionLet);
		console.log(functionConst);*/


// [Section] Nested Functions
	// You can create another function inside a function. 

	function myNewFunction(){
		let name = "Jane";
		console.log(name);

		function nestedFunction(){
			let nestedName = "John";
			console.log(nestedName);
			console.log(name);
		}
		nestedFunction();
	}
	myNewFunction();

	// Function and Global Scoped Variables
		// Global Scoped Variable

		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";
			console.log(globalName);
			console.log(nameInside);
		}
		myNewFunction2();
		

// [Section] Using Alert (pop up)
	// Alert() - allows us to show a small window at the top of our broswer page to show information to our users. As opposed to console.log() which only shows on the console. It also allows us to show a short dialog or instruction to our users. The page will wait until the user dismiss the dialog.


	alert("Hello World"); /*This will run immediately when page loads*/

	// Syntax:
	// alert("<messageInString>");

		function showSampleAlert(){
			alert("Hello, User!");
		}
		showSampleAlert();

	// Notes on use of alert()
		// Show only an alert () for short dialogs or messages to the user
		// Do not overuse alert() because the program or js has to wait for it to be dismissed before continuing.


// [Section] Prompt()
	// Prompt allows us to show a small window at the top of the broswer to gather user input. The input from the prompt() will be returned as a string once the user dismmissed the window.

	//Syntax:
	// prompt("<dialogInString">);

	let samplePrompt = prompt("Enter your name: ");
	console.log(samplePrompt);
	console.log(typeof samplePrompt);

	// It will appear 'null' when the prompt is cancelled.
	let sampleNullPrompt = prompt("Don't enter anything");
	console.log(sampleNullPrompt);
	console.log(typeof sampleNullPrompt);

	function printWelcomeMessages(){
		let firstName = prompt("Enter you First Name: ");
		let lastName = prompt("Enter your Last Name: ");
		console.log("Hello, " + firstName + " " + lastName + "!");
	}

	printWelcomeMessages();


// [Section] Function Naming Convention
	
	// Function names should be definitiveof the task it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}
		getCourses();

	// Avoid generic names to avoid confusion within your code or program

		/*function get(){
			let name = "Jamie";
			console.log(name);
		}
		get();*/

		function getName(){
			let name = "Jamie";
			console.log(name);
		}
		getName();

	// Avoid pointless and inappropriate function names

		function foo(){
			console.log(25%5);
		}
		foo();

	// Name your function in small caps and follow the camelCasing when naming variables and functions

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}
		displayCarInfo();